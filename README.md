# Ansible role to install Jira

I use this role to setup local testing environments.
It can be configured to use different databases on the same or a seperate host.
Installation of proxy and SSL configuration is planned.

## Requirements

root access is required, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook like:

```
- hosts: jira-server
  roles:
    - role: ansible-role-jira
      become: yes
```

## Installation

If not installing to the default directory, e.g. `/etc/ansible`, don't forget the `-p`-Option:

```
  ansible-galaxy install git+https://bitbucket.org/cmbaer/ansible-role-jira -p roles
```

## Postinstall

Open under your choosen systemname: <systemname>:8080

## Role Variables

Available variables can be found in `defaults/main.yml`

## Dependencies

Depending on the database vendor you want to choose.

## Example Playbook

    - hosts: jira-server
      become: yes
      vars_files:
        - vars/all.yml
      roles:
        - { role: jira-ansible-role }

## Example var file vars/all.yml

Jira 8.3.4 with a postgresql db:

    jira_version: 8.3.4
    jira_software_path: "/opt/softwarepackages"

    postgresql_databases:
      - name: "{{jira_database_name}}" 
        lc_collate: C
        lc_ctype: C
        encoding: UNICODE
        owner: "{{jira_dbuser_name}}"

    postgresql_users:
    - name: "{{jira_dbuser_name}}"
      password: "{{jira_dbuser_password}}"

## License

MIT / BSD

## Author Information

This role was created in 2017 by [Christian Baer](https://bitbucket.org/cmbaer/)


